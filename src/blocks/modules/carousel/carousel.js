import "jquery";
import "owl.carousel/dist/owl.carousel";

$(document).ready(function(){
    $('.owl-carousel').owlCarousel({
        margin: 10,
        nav: true,
        dots: false,
        navContainer: '.title__nav',
        items: 1,
    });
});